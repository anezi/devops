FROM alpine:3.10

# To get latest release: https://dl.k8s.io/release/stable.txt

ENV KUBERNETES_VERSION v1.16.2

RUN apk add --no-cache \
        curl \
        gettext \
    && curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBERNETES_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && apk del curl

COPY bin/* /usr/local/bin/